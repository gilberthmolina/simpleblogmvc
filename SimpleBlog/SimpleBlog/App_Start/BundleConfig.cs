﻿using System.Web.Optimization;

namespace SimpleBlog
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Admin scripts
            bundles.Add(new StyleBundle("~/admin/styles")
                .Include("~/content/styles/bootstrap/bootstrap-paper.css")
                .Include("~/content/styles/admin.css")
                .Include("~/content/styles/footer.css"));

            bundles.Add(new ScriptBundle("~/admin/scripts")
                .Include("~/scripts/jquery/jquery-2.2.1.js")
                .Include("~/scripts/jquery/jquery.validate.js")
                .Include("~/scripts/jquery/jquery.validate.unobtrusive.js")
                .Include("~/scripts/bootstrap/bootstrap.js"));

            //Users scripts
            bundles.Add(new StyleBundle("~/styles")
                .Include("~/content/styles/bootstrap/bootstrap-paper.css")
                .Include("~/content/styles/site.css")
                .Include("~/content/styles/footer.css"));

            bundles.Add(new ScriptBundle("~/scripts")
                .Include("~/scripts/jquery/jquery-2.2.1.js")
                .Include("~/scripts/jquery/jquery.validate.js")
                .Include("~/scripts/jquery/jquery.validate.unobtrusive.js")
                .Include("~/scripts/bootstrap/bootstrap.js"));
        }
    }
}